<?php

namespace tests\usertest;

use PHPUnit\Framework\TestCase;

use TEST\tests\User;

class usertest extends TestCase
{
    private User $user;

    protected function setUp(): void
    {
        $this->user = new User('test@test.fr', 'toto', 'tata');
        parent::setUp();
    }

    public function testIsValidNominal()
    {
        $this->assertTrue($this->user->isValid());
    }

    public function testIsNotValidDueToEmptyFirstname()
    {
        $this->user->setFname('');
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidDueToEmptyLastname()
    {
        $this->user->setLname('');
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidDueToEmptyEmail()
    {
        $this->user->setEmail('');
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidDueToBadEmail()
    {
        $this->user->setEmail('pasbon');
        $this->assertFalse($this->user->isValid());
    }


}