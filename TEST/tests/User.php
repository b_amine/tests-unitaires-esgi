<?php

namespace TEST\tests;

use Carbon\Carbon;

class User {

    private string $email;
    private string $nom;
    private string $prenom;
    private Carbon $birthday;

    public function __construct(string $email, string $fname, string $lname )
    {
        $this->email = $email;
        $this->fname = $fname;
        $this->lname = $lname;
    }

    public function isValid()
    {
        return !empty($this->email)
            && !empty($this->lname)
            && !empty($this->fname)
            && filter_var($this->email, FILTER_VALIDATE_EMAIL)
            && $this->birthday->addYears(13)->isBefore(Carbon::now());
    }


    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFname(): string
    {
        return $this->fname;
    }

    /**
     * @param string $fname
     */
    public function setFname(string $fname): void
    {
        $this->fname = $fname;
    }

    /**
     * @return string
     */
    public function getLname(): string
    {
        return $this->lname;
    }

    /**
     * @param string $lname
     */
    public function setLname(string $lname): void
    {
        $this->lname = $lname;
    }

    /**
     * @return Carbon
     */
    public function getBirthday(): Carbon
    {
        return $this->birthday;
    }

    /**
     * @param Carbon $birthday
     */
    public function setBirthday(Carbon $birthday): void
    {
        $this->birthday = $birthday;
    }


}
